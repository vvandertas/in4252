from datetime import datetime
import json

file_name = "10_min_captured_tweets.json"

if __name__ == "__main__":
    # read data from file
    with open(file_name) as f:
        data = json.load(f)

    tweets = data['data']
    print("Processed {0}".format(str(len(tweets))))

    firstTweet = tweets[0]
    lastTweet = tweets[len(tweets)-1]

    print("Timestamp and id first received tweet {0}, {1}".format(datetime.fromtimestamp(int(firstTweet['timestamp'])/1000).strftime('%d-%m-%Y %H:%M:%S'), str(firstTweet['tweet_id'])))
    print("Timestamp and id last received tweet {0}, {1}".format(datetime.fromtimestamp(int(lastTweet['timestamp'])/1000).strftime('%d-%m-%Y %H:%M:%S'), str(firstTweet['tweet_id'])))

    # TODO: Filter by Schiphol coordinates
    # TODO: Filter by language (Dutch and English)