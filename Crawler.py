from __future__ import absolute_import, print_function

import datetime
import atexit

from tweepy.streaming import StreamListener, json
from tweepy import OAuthHandler
from tweepy import Stream

# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key = "nqBXyOmwhlTlOuGpur7Tfw"
consumer_secret = "PDnx65GtmcNTEp8w2MGpoRbd3NpAg12OZBwmkbS8"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token = "218671304-RBg5NfGeqzWyoNPGjGvXfnQff8dxBaaWStKqgA5Q"
access_token_secret = "RFzp8PLgZWkr5SoHlOJ5nX4ROYu15olulKXSpjrKnb38k"

file_name = "test_captured_tweets.json"

_count = 0


@atexit.register
def on_quite():
    end = datetime.datetime.now()
    print("THE END: " + str(end.strftime('%d-%m-%Y %H:%M:%S')))
    print('Total entries processed: {0}'.format(str(_count)))

    with open(file_name, 'a') as tf:
        tf.write('],')
        tf.write(json.dumps('end') + ':' + json.dumps(str(end.strftime('%d-%m-%Y %H:%M:%S'))) + ',')
        tf.write(json.dumps('count') + ':' + json.dumps(str(_count)))
        tf.write('}')


class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.

    """

    def on_data(self, raw_data):
        global _count

        data = json.loads(raw_data)

        if 'delete' in data:
            return True

        processed_data = {
            'tweet_id': data['id'],
            'content': data['text'],
            'language': data['lang'],
            'timestamp': data['timestamp_ms'],
            'coordinates': data['coordinates'],
            'place': data['place']
        }

        # print(data)
        # write the captured tweets to file (append)
        with open(file_name, 'a') as tf:
            if _count == 0:
                tf.write('{' + json.dumps('start') + ':' + json.dumps(str(start)) + ",")
                tf.write(json.dumps('data') + ':')
                tf.write('[')
                tf.write(json.dumps(data)) # TODO: Use processed_data instead
            else:
                tf.write("," + json.dumps(data)) # TODO: Use processed_data instead

        # increment counter
        _count = _count + 1

        if _count % 10 == 0:
            print('Processed {0} entries'.format(str(_count)))

        return True

    def on_error(self, status):
        print(status)


if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    start = datetime.datetime.now()
    print(start.strftime('%d-%m-%Y %H:%M:%S'))

    stream = Stream(auth, l)
    # Only collect tweets that originate from the Amsterdam area
    stream.filter(locations=[4.72, 52.28, 5.04, 52.42])

    try:
        stream.sample()
    except KeyboardInterrupt:
        print("Keyboard interruption")
    except TimeoutError:
        print("TimeoutError")
    except:
        print("Caught an exception")
    # Filter the tweets
